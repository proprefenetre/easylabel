from collections import defaultdict
import json
import more_itertools
import sys
from pathlib import Path
import pytest  # type: ignore
sys.path.insert(0, "/home/eigenraam/projects/repos/easylabel/easylabel/")
from corpus import Corpus, Document, Annotation  # type: ignore


PATTERNS = {"(L|l)orem": "PERS", "ipsum": "ORG", "Ipsum": "LOC"}

LOREM = """Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
minim veniam, quis nostrud exercitation ullamco laboris nisi ut
aliquip ex ea commodo consequat. Duis aute irure dolor in
reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
culpa qui officia deserunt mollit anim id est laborum.""".split("\n")


CORPUS_PATH = Path(Path(__file__).parent, "corpus.json").resolve()


@pytest.fixture
def corpus():
    c = Corpus()
    for l in LOREM:
        c.add(l.strip())
    c.add_patterns(PATTERNS)
    return c


@pytest.fixture
def annotated_corpus(corpus):
    c = corpus
    c.annotate()
    return c


@pytest.fixture
def document(annotated_corpus):
    return annotated_corpus.documents[0]


@pytest.fixture
def annotation(document):
    return document.annotations[0]


def test_adding_texts():
    c = Corpus()
    n = len(LOREM)
    for line in LOREM:
        c.add(line.strip())

    assert c.documents
    assert len(c) == n


def test_add_patterns():
    c = Corpus()
    c.add_patterns(PATTERNS)
    assert c.patterns["Ipsum"] == "LOC"


def test_annotate_corpus(corpus):
    corpus.annotate()
    assert len(corpus.documents[0].annotations) > 0
    assert corpus.documents[0].annotations[0].label == "PERS"


def test_docs_are_Documents(annotated_corpus):
    assert isinstance(annotated_corpus.documents[0], Document)


def test_annotations_are_Annotations(annotated_corpus):
    assert isinstance(annotated_corpus.documents[0].annotations[0], Annotation)


def test_annotated_document_contents(document):
    assert document.text
    assert len(document.annotations) > 0
    assert document.corpus


def test_annotations_contents(annotation):
    assert annotation.start >= 0
    assert annotation.end != annotation.start
    assert annotation.label == "PERS"


def test_corpus_classmethod(annotated_corpus):
    corp = annotated_corpus.as_dict()
    new_corpus = Corpus.from_json(corp)
    assert new_corpus == annotated_corpus


def test_serialization(annotated_corpus):
    with open(CORPUS_PATH, "w") as f:
        json.dump(annotated_corpus.as_dict(), f)

    with open(CORPUS_PATH, "r") as f:
        c = Corpus.from_json(json.load(f))

    assert c == annotated_corpus


def test_Document_show(document):
    assert isinstance(document.show(), list)
    assert [a for a, txt in document.show()] == document.annotations


def test_overwrite_existing_annotations(corpus):
    c = Corpus()
    c.add("Lorem ipsum dolor sit amet, consectetur adipiscing elit")
    c.add_patterns({"ipsum": "NERD"})
    c.documents[0].add_annotation(Annotation(6, 11, "PERS"))
    # doc = c.documents[0]
    # A = Annotation(6, 11, "NERD")
    # for b in doc.annotations:
    #     if b.span() == A.span():
    #         print(f"existing span: {b}")
    #         print(doc.annotations)
    #         doc.annotations = list(more_itertools.replace(doc.annotations, lambda x: x.span() == A.span(), (A,)))
    #         print(doc.annotations)
    c.annotate()
    assert c.documents[0].annotations[0].label == "NERD"


def test_Corpus_update_pattern(corpus):
    corpus.add_patterns({"dolor": "LOC"})
    corpus.update_pattern("dolor", r"\bdolor\b")
    assert corpus.patterns[r"\bdolor\b"]


def test_corpus_update_pattern_KeyError(corpus):
    corpus.add_patterns({"dolor": "LOC"})
    with pytest.raises(KeyError):
        corpus.update_pattern("dlor", r"\bdolor\b")


def test_Document_label_index(document):
    assert isinstance(document.label_index(), defaultdict)
    assert isinstance(document.label_index()["PERS"], list)
    assert isinstance(document.label_index()["PERS"][0], Annotation)


def test_Document_entity_index(document):
    assert isinstance(document.entity_index(), defaultdict)
    assert isinstance(document.entity_index()["Lorem"], list)
    assert isinstance(document.entity_index()["ipsum"], list)
    assert isinstance(document.entity_index()["Lorem"][0], Annotation)
    assert isinstance(document.entity_index()["ipsum"][0], Annotation)
